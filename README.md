# Plan du réseau TCL (non-officiel)

À force de souffrir quand je voyais [le plan des lignes fortes du réseau TCL](https://www.tcl.fr/plans-du-reseau), j'ai décidé d'employer mon temps libre et ma longue expertise professionnelle de 0 années pour composer un plan un peu plus clair et sympathique à regarder.

## Installation/Usage
 - Cloner ce dépôt git.
 - Le plan des lignes fortes, au format SVG, se trouve dans `plan_reseau/lignes_fortes.svg`, et peut être ouvert avec la plupart des navigateurs web, ou avec un éditeur SVG comme [Inkscape](https://inkscape.org/).

## Cahier des charges
 - **Chaque ligne doit être visuellement séparée** : le style du plan d'origine, qui "regroupe" en un seul trait les lignes de tramway circulant sur les mêmes voies, est vecteur de confusion pour les gens qui ne connaissent pas déjà bien le réseau.
 - **Le nombre de logos de lignes doit être minimal** : dans le plan d'origine, l'ensemble des lignes qui passent dans chaque terminus sont mentionnées sous le nom dudit terminus, et les logos sont également copiés tout au long du tracé de la ligne. Ça fait beaucoup trop : il suffit d'indiquer à chaque terminus les lignes *qui y font effectivement terminus*.
 - Les angles à 90° doivent être évités autant que possible.
 - Les noms de station doivent être harmonisés. Sur le plan d'origine il n'y a pas de cohérence, parfois la seconde partie du nom de station est traitée comme un sous-titre (ex: Vaulx-en-Velin – La Soie, Gare Part-Dieu – Villette, Stade de Gerland ‑ Le LOU...) et d'autres fois non (ex: Décines – OL Vallée, Saxe – Gambetta), et d'autres fois encore la seconde partie n'est même pas mentionnée (ex: Laurent Bonnevay). Le nom de station ne comprend PAS DE SOUS-TITRE, et s'il y a un lieu-dit supplémentaire il doit être précisé en-dessous avec un style de texte tout à fait différent (pk pas s'inspirer des cadres bruns de la RATP dans cet usage).
 - L'usage des tirets simples, semi-cadratins et cadratins est régi par une norme claire : tirets pour les noms composés, semi-cadratins pour séparer plusieurs noms dans un nom de station, cadratins pour séparer des noms de station les uns des autres (ex: l'interstation Saint-Clair – Square Brosset — Cité Internationale – Transbordeur).
 - Les noms d'arrêt en gras doivent être réservés aux terminus (qui ne devraient pas tous être entourés d'un rectangle gris/violet, ça prend beaucoup de place...) pour les mettre en valeur.
 - Sur le plan des « lignes fortes » : soit il ne faut afficher aucune ligne de bus C, soit il faut les afficher toutes. La logique du plan d'origine était de représenter les lignes de trolleybus, mais de nombreuses autres lignes sont équipées en trolleybus de nos jours (C1/C2/C3 certes, mais aussi C4/C11/C13/C14/C18).
 - Sur ce type de plan schématique, **la géographie doit s'adapter au tracé des lignes**, pas l'inverse !
