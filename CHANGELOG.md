# Changelog

## 2024-03-31 (Current) - Apostrophes typographiques
 - Les apostrophes utilisées dans les noms des arrêts sont désormais typographiques (`’`) plutôt que droites (`'`).

## 2024-03-31 (9f6b158a) - Ouverture de la ligne B jusqu’à St-Genis-Laval
 - Le tracé de l’extension de la ligne B n’est plus en pointillés puisqu’elle a été mise en service. La Gare d’Oullins n’est donc plus considérée comme un terminus (son nom n’est plus affiché en gras, son icône et les logos affichés ont changé).

## 2023-05-30 (314b977c) - Ajout d'un troisième plan « Lignes fortes et bus renforcés »
 - Le plan `plan_reseau/lignes_fortes_et_trolleybus.svg` a été dupliqué (`plan_reseau/lignes_fortes_et_bus_renforces.svg`) dans le but de faire un plan incluant toutes les lignes de bus renforcé (C1 à C26) et non plus seulement les 8 qui sont des trolleybus. (Pour l'instant les deux plans sont identiques, on y travaille)

## 2023-05-30 (8f37dbd3) - Retouche du tracé du C3
 - Le tracé du C3 a été modifié entre Laurent Bonnevay et Vaulx – Hôtel de Ville – Campus pour permettre de caler "proprement" le nom et les logos de Laurent Bonnevay.

## 2023-05-29 (bf75cc88) - Complétion des tracés des C3 et C14
 - Le tracé des T2 et T4 sur le secteur Jean Macé/Jet d'Eau a été modifié pour laisser plus de place aux noms de station et logos de lignes de Saxe – Gambetta et Jean Macé, et de nombreux noms d'arrêt ont été déplacés en conséquence.
 - La station Jean Macé a désormais une icône de station propre (pour ses 2 terminus de bus + croisement de métro et tram) : `station/tbusC_busC_M.svg`.
 - Idem pour la station Saxe – Gambetta dont l'icône de station dédiée est `station/tbusC_xM.svg`.
 - Les arrêts du C4/C14 Thibaudière et Université – Jean Jaurès ont été ajoutés.
 - Les derniers arrêts du C3 manquants (Terreaux – La Feuillée et Gare St-Paul) ont été ajoutés, et la ligne C14 a été prolongée jusqu'à son terminus (Les Sources) avec tous les arrêts intermédiaires. Les arrêts Valmy et Gare de Vaise, qui sont desservis, ont de nouvelles icônes de station : `station/xM_xC.svg` pour Valmy et `station/tD_xC.svg` pour Gare de Vaise.

## 2023-05-28 (289933b1) - Ajout des arrêts du C18
 - Les tracés des bus C13 et C18 entre Hôtel de Ville – Louis Pradel et Croix-Rousse ont été rapprochés de celui du métro C (pour laisser de la place à gauche pour les arrêts du C3/C14).
 - Les arrêts du bus C18 qui n'étaient pas encore présents sur le plan ont été ajoutés, à l'exception de Musée d'Art Contemporain (que j'espère bien réussir à caler... un jour).
 - L'arrêt Montessuy – Gutemberg du C13 était décalé d'un pixel vers la droite et a été recentré.
 - Quelques points ont été ajoutés au "cahier des charges" du `README.md`.

## 2023-05-28 (0f5a089c) - Ajout d'un second plan « Lignes fortes et trolleybus »
 - Un nouveau plan a été ajouté, `plan_reseau/lignes_fortes_et_trolleybus.svg` ; il correspond peu ou prou au plan officiel de TCL, avec quelques lignes supplémentaires (C4, C11, C13, C14, C18) qui correspondent à toutes les lignes renforcées de trolleybus de l'agglomération, plutôt qu'uniquement les "BHNS" C1/C2/C3 (la différence est toute relative, d'autant plus que le C3 n'est pas en site propre tout du long, et que certaines lignes non présentes sur le plan d'origine (comme le C11) circulent aussi en site propre sur une partie de leur trajet). Le travail dessus n'est pas terminé cependant, il reste quelques stations à ajouter, quelques textes à positionner et quelques cheveux sur mon crâne à arracher. Voici une liste non exhaustive de ce qui change par rapport à `lignes_fortes.svg` :
    - L'ensemble du plan a été décalé vers le bas de 250px, pour permettre d'avoir davantage d'espace pour positionner les (très nombreux) arrêts de bus du nord de l'agglomération
	- La majorité des groupes ont vu leurs éléments répartis en sous-groupes (par exemple `StationsMetroA/B/C/D` dans le groupe `calqueMetro`) pour permettre de les replier (il faudra aussi le faire dans `lignes_fortes.svg` à terme...)
	- La taille des logos a été réduite par rapport à l'autre plan (de 24px à 20px de hauteur) pour permettre de les rentrer dans les zones très denses en lignes comme le secteur de la Part-Dieu.
	- Plusieurs tracés ont été modifiés, comme le projet du T6 Nord qui fait un crochet pour faire ses correspondances avec le C3, le T9 qui se déplace vers le haut/droite pour ses correspondances avec la même ligne.
 - Les conditions des lignes en pointillé ont été ajoutées à la fois dans `lignes_fortes.svg` et `lignes_fortes_et_trolleybus.svg` ("en semaine" pour Meyzieu – Les Panettes, "les jours d'événement" pour Part-Dieu – Villette Sud, et la date d'ouverture prévue pour les lignes en projet/en construction).
 - Les icônes de station en correspondance génériques (`station/x.svg` et `xs.svg`) ont été supprimées, au profit d'icônes qui spécifient la taille du mode de transport le plus "lourd" à cette station : métro (`station/xM.svg`, correspond à `x.svg`), tram (`xT.svg`, correspond à `xs.svg`) ou bus (`xC.svg`, encore plus petit que l'ancien `xs.svg`).
 - De nouvelles icônes de station ont été ajoutées dans `station/`, avec des combinaisons toujours plus farfelues pour figurer les correspondances métro/tram/bus.
 - Deux icônes de demi-flèches (`icone/dir_bus_d.svg` et `dir_bus_g.svg`, respectivement pour *droite* et *gauche*), permettent d'indiquer la direction d'une ligne de bus sur les sections où elle se sépare en deux ou fait une boucle. Elles sont utilisées à de nombreuses reprises sur le tracé de la ligne C13 dans le nouveau plan `lignes_fortes_et_trolleybus.svg`.
 - Les logos des lignes de bus renforcées (`logo/c/1.svg`, `2.svg`, etc...) ont été légèrement redimensionnés pour que leurs dimensions soient rondes. C'est un peu fait à l'arrache pour l'instant au niveau des alignements, il faudra les refaire proprement un de ces jours.

## 2023-05-26 (2ff15675) - Icônes SNCF/Rhônexpress/Parking relais
 - La police des logos de ligne/mode de transport et celle des noms de station ont été remplacées par la police [Overpass](https://overpassfont.org/).
 - Deux icônes ont été reproduites depuis le plan d'origine, pour les parcs relais voiture et vélo (`icone/parc_relais.svg` et `parc_relais_velo.svg`). Deux versions alternatives (avec des icônes plus grandes et le texte *P+R* en plus petit) ont aussi été créées, mais elles ne me satisfont pas suffisamment pour remplacer celles d'origine, donc en attendant de les avoir améliorées je les ai juste nommées `icone/parc_relais_alt.svg` et `parc_relais_velo_alt.svg`.
 - Deux logos ont été reproduits/adaptés, celui de la SNCF pour indiquer les gares (`logo/sncf.svg`) et celui du Rhônexpress (`logo/rhonexpress.svg`). Ils ne sont pas des copies ultra fidèles des originaux (et TODO : il faudra peut-être remplacer le logo SNCF biscornu par un carré bleu avec un train dedans, dans le même style que les parcs relais).
 - Tous les parcs relais (voiture/vélo) et gares SNCF sont désormais indiquées sur le plan avec les icônes nouvellement ajoutées. Le placement de ces icônes n'est pas forcément définitif, il y en a qui ne me satisfont pas des masses (c'est surtout que les logos SNCF et Rhônexpress s'accordent très mal avec les logos des lignes TCL). La norme qui me paraît la plus lisible est celle que j'ai utilisée pour St-Genis-Laval et Hôpital Feyzin–Vénissieux : les logos des lignes TCL, puis le nom du terminus, puis les logos parking/SNCF (mais il faut avoir la place...). Pour les stations sans terminus, j'ai essayé de placer après le texte s'il était à droite de la station, et avant s'il était à gauche (pour que le texte reste collé à l'icône de station et que les icônes de parking restent vers l'extérieur).
 - Tous les noms de station manquants et les icônes de terminus correspondantes ont été ajoutées, donc ceux des T6/T7/T9/T10 et l'aéroport St-Exupéry.
 - La station Vieux Lyon a désormais une icône de station avec les points de terminus des funiculaires au lieu d'une icône de correspondance générique (c'était une erreur). La nouvelle icône s'appelle `station/tF1_F2_M.svg`. Le tracé du funiculaire F2 a été légèrement décalé pour lui permettre d'atteindre ce terminus à 90° plutôt que 45°.
 - La station Gare Part-Dieu – Villette a été réhaussée (pour permettre l'ajout de Part-Dieu – Villette Sud qui avait été oubliée) et est désormais liée (par un trait blanc) avec la station Gare Part-Dieu – Vivier Merle.
 - Deux « liaisons urbaines » ont été ajoutées :
    - la première (entre Thiers – Lafayette et Gare Part-Dieu – Villette) existait sur le plan d'origine : elle se justifie car c'est une voie protégée (uniquement vélos/piétons/trams), au contraire de la liaison entre Gare Part-Dieu – Villette et Part-Dieu – Villette Sud qui traverse un axe majeur et ne devrait à mon sens pas être indiquée
	- la seconde (entre Perrache et Place des Archives) n'existe pas sur le plan d'origine : à la place, le logo SNCF est dupliqué et affiché également à la station Place des Archives. Il me paraissait plus simple de simplement indiquer une liaison pédestre entre les 2 stations et de ne pas afficher en double l'icône SNCF qui jure pas mal avec le design des autres logos
 - Le nom et les logos de ligne de la station Charpennes – Charles Hernu ont été déplacés : ils étaient en bas à droite de l'icône de station, ils sont désormais en haut à gauche. Cela permet d'avoir plus de place pour les noms d'arrêt situés en-dessous (Collège Bellecombe/Thiers – Lafayette/Part-Dieu – Villette...). En contrepartie, les stations du T1/T4 Le Tonkin, Condorcet et Université Lyon 1 ont été resserrés légèrement vers La Doua – Gaston Berger.
 - Les arrêts Quai Claude Bernard, Centre Berthelot et Rue de l'Université ont été un peu déplacés (ainsi que leurs noms) pour permettre de passer le nom « Jean Macé » à gauche plutôt qu'à droite (puisqu'on a besoin de l'espace à droite pour mettre le logo SNCF, et qu'il est impossible de le mettre plutôt au-dessus/en-dessous du texte sans que ça soit très moche... et FRANCHEMENT j'ai cherché).
 - L'arrêt du T2/T5 Ambroise Paré a été légèrement décalé vers la gauche pour éviter la confusion avec le nom de l'arrêt Desgenettes.
 - Le terminus du T2 Saint-Priest – Bel Air a été descendu pour laisser de la place au nom de station.
 - L'arrêt Monplaisir – Lumière a été remonté, et son nom est repassé sur une seule ligne (je l'avais mis sur 2 pour laisser de la place aux autres noms de station sur la droite, mais il s'avère qu'on en a moins utilisé que prévu...).
 - L'arrêt Place Guichard a été correctement renommé Place Guichard – Bourse du Travail.

## 2023-05-25 (8e5cfb0f) - Réorganisation secteur Grange Blanche / Noms de station T1 à T5
 - La zone autour de Grange Blanche a été réorganisée pour permettre à la fois d'afficher le point de terminus du T5 et le nom des stations alentour.
 - Ajout de l'icône de station `station/x_x_rot.svg`, destinée à indiquer une correspondance entre plusieurs lignes se croisant, dont au moins 2 parallèles (elle est utilisée à l'arrêt Desgenettes, qui voit se croiser à 45° les lignes T6 et T2/T5).
 - Ajout de l'icône de station `station/tT5_x_rot.svg`, utilisée pour la station Grange Blanche : selon la même logique que `x_x_rot`, il s'agit d'une icône plus allongée que la normale (pour être utilisée à 45°) comprenant le terminus du T5 ainsi qu'un autre tram sans terminus.
 - Les noms de station et logos des lignes ont été ajoutés pour l'ensemble des tracés des lignes T1, T2, T3, T4 et T5, ainsi que sur la partie du métro D non terminée dans le commit précédent.
 - L'interstation Meyzieu–Z.I. — Meyzieu–Les Panettes a été rallongé pour permettre aux deux noms de terminus d'avoir suffisamment de place.
 - Plusieurs autres arrêts ont été déplacés pour se jouer des contraintes de place, comme Archives Départementales, les quelques stations du T4 avant Hôpital Feyzin–Vénissieux, ou Reconnaissance–Balzac.
 - Quelques lignes ont été ajoutées à `plan_reseau/plan_reseau.css` pour gérer le texte en indice (qui figure dans les noms de mairie comme « Mairie du 8ème »).

## 2023-05-25 (b9126fab) - Espacement de l'est du plan des lignes fortes
 - Élargissement du plan des lignes fortes de 1536x1536 à 2048x2048 pour avoir davantage de marge à l'est, et de l'espace pour placer le cadre d'information.
 - La ligne A a été allongée à l'est (à partir de Charpennes) pour rendre les alignements avec le T3 et le T6 Nord plus élégants : le tracé du T6 Nord a également été modifié et fait moins de virages. Au nord de Charpennes, le tracé des T1, T4 et T9 a également été allongé et les stations Tonkin à Gaston Berger sont moins collées les unes aux autres.
 - Le tracé du T1 autour de Part-Dieu a été retouché pour laisser de la place aux noms des stations.
 - Le tracé des funiculaires a changé, également pour laisser de la place aux noms des stations : les deux lignes se trouvent désormais à gauche du tracé du métro D, plutôt que de part et d'autre de celui-ci.
 - Le nom de la plupart des stations des métros ont été ajoutés, ainsi que les logos des modes de transport et lignes (uniquement les lignes faisant terminus dans cette station, plutôt que toutes celles qui y passent, pour alléger le plan).
 - L'icône de station `station/tT1_x_T6.svg` a été renommée `tT1_M_T6` car l'espace laissé au milieu est plus large (il correspond à une ligne de métro) alors que dans tous les autres cas, les `x` signalent un espace vide plus étroit (de la largeur d'une ligne de tramway).
 - Une nouvelle icône de station a été ajoutée pour le terminus du métro C en correspondance avec le métro A : `station/tC_M.svg`.
 - Les logos des lignes de tramway T9 et T10, qui avaient été oubliées, ont été ajoutées (`logo/t/9.svg` et `10.svg`).

## 2023-05-24 (0347718d) - Version initiale
 - Ébauche de plan des lignes fortes du réseau TCL : le style est dans `plan_reseau/plan_reseau.css`, et le plan proprement dit est dans `plan_reseau/lignes_fortes.svg`.
 - Création des logos des modes de transport (`logo/bus.svg`, `c.svg`, `f.svg`, `m.svg` et `t.svg`, respectivement pour les bus, les bus renforcés, les funiculaires, les métros et les tramways). Création de tous les logos des lignes de bus renforcé (`logo/c/`), de funiculaire (`logo/f/1` et `2.svg`), de métro (`logo/m/A`, `B`, `C`, `D.svg`) et de tram (`logo/t/1`, `2`, ..., `10.svg`). Les seuls logos de ligne manquants sont ceux des lignes de bus classiques, qui ne sont pas utilisés sur le plan des « lignes fortes » d'origine.
 - Création de nombreuses icônes de station :
	- les stations simples sans correspondance des lignes de métro (`station/A`, `B`, `C`, `D.svg`), funiculaire (`station/F1.svg`) et tram (`station/T1`, `T2`, `T3`..., `T10.svg`)
	- la correspondance générique circulaire (`station/x.svg`) utilisée pour des lignes qui se croisent (ex: Bellecour, Jean Macé...), ainsi qu'une version plus petite (`station/xs.svg`) pour des croisements de trams (ex: Jet d'eau–Mendès France)
	- les correspondances génériques allongées (`station/x_x` et `x_x_x.svg`), utilisées pour des lignes faisant correspondance en parallèle (comme beaucoup d'arrêts de tram)
    - les terminus sans correspondance des différentes lignes de métro (`station/tA.svg`, `tB.svg`, `tC.svg`, `tD.svg`), funiculaire (`station/tF1`, `tF2.svg`) et tram (`station/tT1`, `tT2`, `tT3`..., `tT10.svg`, et `tRhonexpress.svg`), qui incluent un point coloré de la couleur de la ligne
	- les terminus en correspondance, qui incluent un ou des points colorés pour la ou les lignes faisant terminus et des espaces vides pour les lignes ne faisant qu'y passer, sont nommés par la liste des lignes qui y sont représentées, de gauche à droite (une ligne ne faisant pas terminus étant désignée par un `x`) : `station/tA_x_x.svg` (Perrache: Métro A + 2 lignes sans terminus), `tB_x_x_T9.svg` (Charpennes: Métro B + 2 lignes sans terminus + T9), `tD_x_T10.svg` (Gare de Vénissieux: Métro D + ligne sans terminus + T10), `tT1_x_T6.svg`, `tT2_x.svg`, `tT3_x.svg`, `tT9_A_T7_x_x.svg`, `tT9_x_T4_x.svg`, `tT10_x.svg`